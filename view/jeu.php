<div id="game" class="mt-5">

  <div class="row">
    <div id="alert" class="col-6 offset-3"></div>
  </div>

  <div class="row justify-content-between">
    <div id="characterContainer" class="col-6">
      <div id="damage-income"></div>
      <img id="character-skin" src="../view/img/character.png"/>
    </div>
    <div id="ennemi" class="col-6">
      <div id="damage-display"></div>
      <div id="damage-skin"></div>
      <img id="ennemi-skin" src="../view/img/ennemi.png"/>
    </div>
  </div>

  <div class="row mt-4 justify-content-around">
    <div class="col-3">
      <div id="status">
        <div class="progress">
          <div id="hp">
            <div id="hp-text">999/999 HP</div>
          </div>
        </div>
        <div class="progress">
          <div id="mana">
            <div id="mana-text">20/20 MP</div>
          </div>
        </div>
        <div class="progress">
          <div id="atb"></div>
        </div>
      </div>
    </div>

    <div class="col-3">
        <div id="ennemi-pv">
          <div id="scan-display"></div>
        </div>
        <div id="barre1" class="progress-bar">
          <div id="atb-ennemi"></div>
        </div>
        <div id="barre2" class="progress-bar">
          <div id="special-ennemi"></div>
        </div>
        <div id="income-skin"></div>
      </div>
  </div>

  <div class="row justify-content-between">
    <div class="col-6">
      <div class="row">
      <div>
        <ul id="menu-action" class="mt-4">
          <li id="attaque">Attaque</li>
          <li id="magie">Magie</li>
          <li id="defense">Defense</li>
          <li id="objets">Objets</li>
        </ul>

        <ul id="spell-list" class="mt-4">
          <div class="tooltips">
            <li id="feu">Feu</li>
            <span class="tooltiptext">Inflige de x à x points de dégâts de feu à la cible.<br />Coût : 3MP</span>
          </div>
          <div class="tooltips">
            <li id="givre">Givre</li>
            <span class="tooltiptext">Inflige de x à x points de dégâts de givre à la cible.<br />Coût : 2MP</span>
          </div>
          <div class="tooltips">
            <li id="foudre">Éclair</li>
            <span class="tooltiptext">Inflige de x à x points de dégâts de foudre à la cible.<br />Coût : 5MP</span>
          </div>
          <div class="tooltips">
            <li id="scan">Scan</li>
            <span class="tooltiptext">Affiche le nombre de points de vie restants de la cible.<br />Coût : 1MP</span>
          </div>
          <div class="tooltips">
            <li id="soin">Soin</li>
            <span class="tooltiptext">Vous soigne à hauteur de x à x points de vie. <br />Coût 4MP</span>
          </div>
        </ul>

      </div>
    </div>
  </div>

    <div class="col-6">
      <ul id="choix-logs">
        <li>Dégâts infligés</li>
        <li>Dégâts reçus</li>
        <li>Masquer</li>
      </ul>

      <div id="logs" class="col-12">
        <div id="entrants">
        </div>
        <div id="sortants">
        </div>
      </div>
    </div>
  </div>




</div>

<!-- <div class="row">



  <div id="win">
    <h1>Vous avez vaincu votre ennemi !</h1>
  </div>

  <div id="restart">
    <button>Recommencer</button>
    <button>Quitter</button>
  </div> -->
