<?php require_once ("../model/include/class/Users.php");?>
<header>
  <nav class="container">
    <div class="row justify-content-around">
      <ul class="col-12">
        <a href="#"><li class="col-3">Accueil</a></li></a>
        <a href="#"><li class="col-3">Leaderboard</a></li></a>
        <a href="#"><li class="col-3">Boutique</a></li></a>
        <a href="#"><li class="col-3">Contact</a></li></a>
      </ul>
    </div>
  </nav>

  <a class="btn btn-info mt-2" href="../model/include/crud.php">Editer la base de donnée utilisateur</a>
  <a class="btn btn-info mt-2" href="../model/include/login.php">Connexion</a>
  <a class="btn btn-info mt-2" href="../model/include/logout.php">Déconnexion</a>




  <div id="index-main-container" class="container mt-5">
    <div class="row">
      <div class="col mt-5">
        <div>
          <h1>
            <?php if (!isset($_SESSION)){
              echo 'Connectez vous pour avoir accès à l\'intégralité du site !';
            }else {
              echo $user->_nom.', garder votre compagnon pour l\'eternité n\'a jamais été aussi simple ! ';
            }
            ?></h1>
          </header>
