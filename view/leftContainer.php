<div id="nom" class="row">
    <h1>Jan VERMEULEN</h1>
  </div>

  <div id="titre" class="row">
    <h1 class="bigTitle"><b>Développeur Junior</b></h1>
  </div>
<!-- INTRODUCTION -->
  <div class="row">
    <p id="intro">
      Je suis une personne motivée, sérieuse, dynamique, à l’écoute des autres,
      je possède un fort esprit d’équipe.Toujours souriant, j’ai un besoin de
      satisfaire le client en répondant au mieux à ses attentes. Ayant pris la
      décision de me réorienter dans un domaine qui me passionne, je recherche
      une alternance dans le développement web ou la programmation pour l'année
      2018-2019 pour pouvoir intégrer une école de programmation.
    </p>
  </div>
  <!-- Compétences Web -->
  <div class="row">
    <div class="col-6">
      <h2 id="webComp"> Compétence Web</h2>
    </div>

    <div class="col-6">
  </div>

</div>
