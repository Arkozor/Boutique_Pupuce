window.onload = function frame(){
  var elem =  document.getElementById("atb-ennemi");
  var width = 1;
  var id = setInterval(frame, 70);
  function frame(){
    if(width >= 100){
    }else{
      width++;
      elem.style.width = width + "%";
    }
  }
};

//////////////////////////////A C T I O N - E N N E M I/////////////////////////

var interval = setInterval(function actionEnnemi(){
  ////ATB ENNEMI////
  var elem =  document.getElementById("atb-ennemi");
  var width = 1;
  var id = setInterval(frame, 70);
  function frame(){
    if(width >= 100){
      clearInterval(id);
    }else{
      width++;
      elem.style.width = width + "%";
    }
  }
  /////////////////
var date = new Date();
var heure = date.getHours();
var min = date.getMinutes();
var sec = date.getSeconds();
var attaque1 = (Math.floor(Math.random() * 75)+100);
var attaque2 = (Math.floor(Math.random() * 75)+200);
var attaque3 = (Math.floor(Math.random() * 75)+800);
var attaqueEnnemi = (Math.floor(Math.random() * 10));
if(attaqueEnnemi>0 && attaqueEnnemi<=8){
  $("#character-skin").animate({left: '-10%'}, 500).animate({left: '0px'}, 500);
  hp=hp-attaque1;
  var elemHp =  document.getElementById("hp");
  var widthHp = hp;
  elemHp.style.width = widthHp/9.9 + "%";
  $("#hp-text").html(hp+"/999 HP"); // Actualise les pv de la cible
  $("#damage-income").html(attaque1).fadeIn(500).fadeOut(1000); // Affiche l'attaque dans la div display + "!"
  $("#damage-income").css("color", "white").css("font-size","7em");
  $("#entrants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez reçu : "+attaque1+" points de dégâts "+"(Attaque) "+" ["+hp+"/999 HP]</p>");
} if(attaqueEnnemi>8 && attaqueEnnemi<10){
  $("#character-skin").animate({left: '-20%'}, 500).animate({left: '0px'}, 500);
  hp=hp-attaque2;
  var elemHp =  document.getElementById("hp");
  var widthHp = hp;
  elemHp.style.width = widthHp/9.9 + "%";
  $("#hp-text").html(hp+"/999 HP"); // Actualise les pv de la cible
  $("#damage-income").html(attaque2).fadeIn(500).fadeOut(1000); // Affiche l'attaque dans la div display + "!"
  $("#damage-income").css("color", "yellow").css("font-size","7em");
  $("#entrants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez reçu : "+attaque2+" points de dégâts "+" (Frappe Mortelle) "+" ["+hp+"/999 HP]</p>");
}
  if(attaqueEnnemi == 0){
  ////A T B ATTAQUE SPECIALE ////
  var elem2 =  document.getElementById("special-ennemi");
  var width2 = 1;
  var id2 = setInterval(frame2, 100);
  function frame2(){
    if(width2 >= 100){
      clearInterval(id2);
      elem2.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      elem2.style.backgroundColor= "red";
      width2++;
      elem2.style.width = width2 + "%";
    }
  }
  ///////////////////////////
    $("#alert").html("L'ennemi prépare une attaque<br /> DEFENDEZ VOUS !").fadeToggle(5000);
    setTimeout(function(attaque){
    if(defense != true){
      $("#character-skin").animate({left: '-5%'}, 500).animate({left: '0px'}, 500);
    hp=(hp-attaque3);
    var elemHp =  document.getElementById("hp");
    var widthHp = hp;
    elemHp.style.width = widthHp/9.9 + "%";
    $("#hp").html(hp+"/999 HP"); // Actualise les pv de la cible
    $("#damage-income").html(attaque3).fadeIn(500).fadeOut(1000); // Affiche l'attaque dans la div display + "!"
    $("#entrants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+"Vous avez reçu : "+ attaque3 +" points de dégâts"+" (Attaque Finale) "+" ["+hp+"/999 HP]</p>");//entrants Dégâts reçus
    $("#damage-income").css("color", "red").css("font-size","7em");
  }
  else{
    $("#character-skin").animate({left: '-40%'}, 500).animate({left: '0px'}, 500);
    var absorb = (Math.floor(Math.random() * 600)+200);
    hp=hp-(attaque3-absorb);
    var elemHp =  document.getElementById("hp");
    var widthHp = hp;
    elemHp.style.width = widthHp/9.9 + "%";
    $("#hp-text").html(hp+"/999 HP");
    $("#damage-income").html(attaque3-absorb).fadeIn(500).fadeOut(1000); // Affiche l'attaque dans la div display + "!"
    $("#entrants").append("<p>"+heure+":"+min+":"+sec+"&nbsp;"+"Vous avez reçu : ("+(attaque3-absorb)+") points de dégâts ("+absorb+"absorbés)"+" ["+hp+"/999 HP]</p>");//entrants Dégâts reçus
    $("#damage-income").css("color", "grey").css("font-size","7em");
    $("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Défense désactivé.</p>");
    defense = false;
  }},10000)
}
},7000);
////////////////////////////////////////////////////////////////////////////////
