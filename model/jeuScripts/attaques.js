///////////////M E C A N I Q U E - A T T A Q U E////////////////////////////////

$("#attaque").mousedown("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
  $("#menu-action").toggle();
  setTimeout(function(){
    $("#menu-action").fadeToggle()},5000);
  var degats = (Math.floor(Math.random() * 75)+100*armor); //Défini la valeur de l'degats selon le modèle (random base 75)+(100dmg/l'armure)
  var attaque = (Math.floor(Math.random() * 10)); // Chance de coup critique
  enrage=enrage+1;
  $("#damage-display").html(degats).fadeIn(1000).fadeOut(1000);//Montre les dégâts infligés
  $("#damage-display").css("color", "white").css("font-size","5em");//Defini la couleur des dégats et leur taille

  if (attaque>1 && attaque<7){
    $("#character-skin").animate({left: '40%'},300).animate({left: '0'},300);
    pv=(pv-degats); //PV actuel de l'ennemi
    enrage=enrage+1;
    $("#damage-display").html(degats).fadeIn(1000).fadeOut(1000);//Montre les dégâts infligés
    $("#sortants").append("<p>"+heure+":"+min+":"+sec+"&nbsp;"+" Vous avez infligé"+"&nbsp"+degats+" points de dégâts à la cible.</p>");
    $("#damage-skin").html('<img src="../view/img/degats1.png"/>').fadeIn().fadeOut(1000);
  }
  if (attaque<=1){ //Si le random de chances de "rater" est inférieur à 2/10, le coup rate
    $("#damage-display").html("Raté"); //Affiche "Raté" dans la bulle de dégâts
      $("#character-skin").animate({bottom: '10%'}).animate({bottom: '0%'},200)
    $("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+"Votre attaque a manqué la cible.</p>");
  }
  if (attaque>=7){ //Si le random des chances de coups critiques est supérieur à la valeur lance le script
      $("#character-skin").animate({left: '40%'}).animate({bottom: '50%'},300).animate({bottom: '0%'},100).animate({left: '0'},300);
    cc = degats*3;//Redéfinis la valeur d'degats à degats*3 uniquement dans le cadre d'un coup critique
    pv = pv-cc+degats;
    $("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+"Vous avez infligé"+"&nbsp"+cc+" points de dégâts à la cible (Critique !).</p>");
    $("#damage-display").html(cc + " &nbsp;!"); // Affiche la valeur de l'degats dans la bulle de dégâts + "!"
    $("#damage-display").css("color", "yellow").css("font-size","7em"); // La couleur du texte des dégâts devient rouge et plus gros
    enrage=enrage+2;
  }
  if (pv<=degats){ //si les pv sont inférieurs au dégâts de l'degats, l'ennemi meurt
  $("#menu-action, #ennemi-life").css("display", "none"); //Masque le menu action
  $("#win").css("display", "block"); //Montre le message "Win"
  $("#restart").css("display", "block"); //Affiche les boutons restart/quit
}
  if ($("#spell-list").css("display") == "block"){
  $("#spell-list").toggle();
}
var elemPv =  document.getElementById("ennemi-pv");
var widthPv = pv;
elemPv.style.width = (widthPv/5000)*100 + "%";
if(scan = true){
$("#scan-display").html(pv +"/5000").css("color","white"); // Affiche les points de vies actuels
}});
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////D E F E N S E //////////////////////////
$("#defense").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
  defense = true;
  $("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Défense activée, prochaine attaque finale réduite. </p>");
  $("#menu-action").toggle();
  if ($("#spell-list").css("display") == "block"){
  $("#spell-list").toggle()};
  setTimeout(function(){
  $("#menu-action").fadeToggle()},5000);

});

////////////////////////////////////////////////////////////////////////////////









////////////////////////////////M A G I E //////////////////////////////////////


//////////////////////////// F E U

$("#feu").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
if(mp<3){$("#feu").hover(off("click"))};
var feu = (Math.floor(Math.random() * 300)+300);
pv= pv - feu;
$("#damage-skin").html('<img src="../view/img/feu.gif"/>').fadeIn().fadeOut(1500);
$("#damage-display").html(feu).fadeIn(1000).fadeOut(1000); // Affiche l'degats dans la div display + "!"
$("#damage-display").css("color", "red").css("font-size","7em");
$("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez infligé"+"&nbsp"+feu+" points de dégâts de feu à la cible.</p>");
mp = mp-3;
var elemMp =  document.getElementById("mana");
var widthMp = mp;
elemMp.style.width = widthMp*5 + "%";
enrage=enrage+2;
if (pv <= feu){ //si les pv sont inférieurs au dégâts de l'degats, l'ennemi meurt
$("#menu-action, #ennemi-life").css("display", "none"); //Masque le menu action
$("#win").css("display", "block"); //Montre le message "Win"
$("#restart").css("display", "block"); //Affiche les boutons restart/quit
}
$("#spell-list,#menu-action").toggle();
setTimeout(function(){
  $("#menu-action").fadeToggle()},5000);
$("#mana-text").html(mp+"/20 MP");
var elemPv =  document.getElementById("scan-display");
var widthPv = pv;
elemPv.style.width = widthPv/125 + "%";
if(scan = true){
$("#scan-display").html(pv +"/5000").css("color","white"); // Affiche les points de vies actuels
}
});
////////////////////////////////////////////////////////////////////////////////


//////////////////////////// G I V R E

$("#givre").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
if(mp<2){$("#givre").hover(off("click"))};
var givre = (Math.floor(Math.random() * 50)+350);
pv= pv - givre;
$("#damage-display").html(givre).fadeIn(1000).fadeOut(1000); // Affiche l'degats dans la div display + "!"
$("#damage-display").css("color", "blue").css("font-size","7em");
$("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez infligé"+"&nbsp"+givre+" points de dégâts de givre à la cible.</p>");
mp = mp-2;
var elemMp =  document.getElementById("mana");
var widthMp = mp;
elemMp.style.width = widthMp*5 + "%";
enrage=enrage+2;
if (pv<= givre){ //si les pv sont inférieurs au dégâts de l'degats, l'ennemi meurt
$("#menu-action, #ennemi-life").css("display", "none"); //Masque le menu action
$("#win").css("display", "block"); //Montre le message "Win"
$("#restart").css("display", "block"); //Affiche les boutons restart/quit
}
$("#spell-list, #menu-action").toggle();
setTimeout(function(){
  $("#menu-action").fadeToggle()},5000);
$("#mana-text").html(mp+"/20 MP");
var elemPv =  document.getElementById("scan-display");
var widthPv = pv;
elemPv.style.width = widthPv/125 + "%";
if(scan = true){
$("#scan-display").html(pv +"/5000").css("color","white"); // Affiche les points de vies actuels
}
});

/////////////////////////////F O U D R E
$("#foudre").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
if(mp<5){$("#foudre").hover(off("click"))};
var foudre = (Math.floor(Math.random() * 400)+300);
pv = pv - foudre;
enrage=enrage+10;
$("#damage-skin").html('<img src="../view/img/foudre.gif"/>').fadeIn().fadeOut(1500);
$("#damage-display").html(foudre).fadeIn(1000).fadeOut(1000); // Affiche l'degats dans la div display + "!"
$("#damage-display").css("color", "yellow").css("font-size","7em");
$("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez infligé"+"&nbsp"+foudre+" points de dégâts de foudre à la cible.</p>");
mp = mp-5;
var elemMp =  document.getElementById("mana");
var widthMp = mp;
elemMp.style.width = widthMp*5 + "%";
if (pv <= foudre){ //si les pv sont inférieurs au dégâts de l'degats, l'ennemi meurt
$("#menu-action, #ennemi-life").css("display", "none"); //Masque le menu action
$("#win").css("display", "block"); //Montre le message "Win"
$("#restart").css("display", "block"); //Affiche les boutons restart/quit
}
$("#spell-list, #menu-action").toggle();
setTimeout(function(){
  $("#menu-action").fadeToggle()
$("#damage-skin").empty()},5000);
$("#mana-text").html(mp+"/20 MP");
var elemPv =  document.getElementById("scan-display");
var widthPv = pv;
elemPv.style.width = widthPv/125 + "%";
if(scan = true){
$("#scan-display").html(pv +"/5000").css("color","white"); // Affiche les points de vies actuels
}
});
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////S C A N////////////////////////////////////////
$("#scan").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
if(mp<1){$("#scan").hover(off("click"))};
$("#scan-display").html(pv +"/5000").css("color","white").css("display","block"); // Affiche les points de vies actuels
$("#sortants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez scanné la cible : "+pv+"/5000 PV.</p>");
mp = mp-1;
scan = true;
var elemMp =  document.getElementById("mana");
var widthMp = mp;
elemMp.style.width = widthMp*5 + "%";
$("#spell-list, #menu-action").toggle();
$("#mana-text").html(mp+"/20 MP");
setTimeout(function(){
  $("#menu-action").fadeToggle()},5000);
});
////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////S O I N////////////////////////////////////////
$("#soin").on("click", function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0.7)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  var date = new Date();
  var heure = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
if(mp<4){$("#soin").hover(off("click"))};
var soin = (Math.floor(Math.random() * 50)+300);
if(mp>=4){
$("#damage-income").html(soin).css("color","green").fadeIn(1000).fadeOut(5000); // Affiche les points de vies actuels
if((hp+soin)>999){
  hp = 999}
  else{
hp = hp+soin;
}
$("#income-skin").html('<img src="../view/img/heal2.gif"/>').fadeIn().fadeOut(1500);
var elemHp =  document.getElementById("hp");
var widthHp = hp;
elemHp.style.width = widthHp/9.9 + "%";
mp = mp-4;
var elemMp =  document.getElementById("mana");
var widthMp = mp;
elemMp.style.width = widthMp*5 + "%";
$("#hp-text").html(hp+"/999 HP");
$("#entrants").append("<p>"+heure +":"+min+":"+sec+"&nbsp;"+" Vous avez récupéré "+soin+" HP"+" (Soin) "+" ["+hp+"/999 HP]</p>");
}
$("#spell-list, #menu-action").fadeToggle(500);
setTimeout(function(){
  $("#menu-action").fadeToggle()},5000);
$("#mana-text").html(mp+"/20 MP");
});


//////////////////////////// I T E M ///////////////////////////////////////////
$("#objets").one("click", (function(){
  var atb =  document.getElementById("atb");
  var width3 = 1;
  var id3 = setInterval(frame3, 50);
  function frame3(){
    if(width3 >= 100){
      clearInterval(id3);
      atb.style.backgroundColor= "rgba(15, 66, 169, 0)";
    }else{
      atb.style.backgroundColor= "#f4ee42";
      width3++;
      atb.style.width = width3 + "%";
    }
  }
  mp = mp-mp+20;
  $("#mana-text").html(mp+"/20 MP");
}));
