/////////////// V A R I A B L E S   &&  F O N C T I O N S /////////////////////
var pv = 5000;
var ap = 10;
var pvplus = pv*1.5;
var armor = 0.4;
var mp = 20;
var hp = 999;
var hpmoins = hp*0.7;
var defense = false;
var enrage = 0;
scan = false;

////////////////////////////////////////////////////////////////////////////////

$("#test1").on("click", function(){
  console.log(enrage);
})
////////////////////////////////I N T E R F A C E //////////////////////////////

$("#magie").on("click",function(){ // Permet d'afficher ou de masquer la spell-list au click de magie
  $("#spell-list").toggle(200);
});
$("#choix-logs li").eq(0).on("click", function(){
  $("#sortants").css("display", "block");
  $("#entrants").css("display","none");
});
$("#choix-logs li").eq(1).on("click", function(){
  $("#entrants").css("display", "block")
  $("#sortants").css("display","none");
});
$("#choix-logs li").eq(2).on("mousedown", function(){
  if($("#choix-logs li").eq(2).html() == "Masquer"){
      $("#choix-logs li").eq(2).html("&nbsp;Afficher&nbsp;").css("transform","rotate(-90deg)").css("margin-top","10px").css("font-size","12px");
      $("#logs").toggle();
      $("#choix-logs li").eq(1).toggle();
      $("#choix-logs li").eq(0).toggle();
    }else {
          $("#choix-logs li").eq(2).html("Masquer").css("font-size", "12px").css("transform","rotate(0deg)").css("margin-top","0px");
          $("#logs").toggle();
          $("#choix-logs li").eq(1).toggle();
          $("#choix-logs li").eq(0).toggle();
      }
});
////////////////////////////////////////////////////////////////////////////////




////////////////////////// F I N - C O M B A T /////////////////////////////////
$("#restart").eq(0).on("click", function(){//cible le premier enfant de la liste restart
  pv = pvplus;
  armor = 0.2;
  mp = 20;
  $("#mana").html(mp+"/20");
  $("#ennemi-life").html(pv);
  $("#menu-action, #ennemi-life").css("display","block");
  $("#restart").css("display","none");
  $("#win").css("display","none");
});
$("#restart button").eq(1).on("click", function(){//cible le deuxième enfant de la liste restart
  $("#game").css("display","none");
});
////////////////////////////////////////////////////////////////////////////////
