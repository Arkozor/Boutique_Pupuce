<?php
require 'autoloader.php';
Autoloader::register(); 

if ( !empty($_POST)) {
  // keep track validation errors
  $nameError = null;
  $emailError = null;
  $mdpError = null;

  // keep track post values
  $name = $_POST['name'];
  $email = $_POST['email'];
  $mdp = $_POST['mdp'];
  $login = $_POST['login'];

  // validate input
  $valid = true;
  if (empty($name)) {
    $nameError = 'Please enter Name';
    $valid = false;
  }

  if (empty($email)) {
    $emailError = 'Please enter Email Address';
    $valid = false;
  } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
    $emailError = 'Please enter a valid Email Address';
    $valid = false;
  }

  if (empty($mdp)) {
    $mdpError = 'Please enter Password';
    $valid = false;
  }

  // insert data
  if ($valid) {
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO users (Users_Nom,Users_Login,Users_Email,Users_Mdp) values(?, ?, ?, ?)";
    $q = $pdo->prepare($sql);
    $q->execute(array($name,$login,$email,$mdp));
    Database::disconnect();
    header("Location: ../../control/index.php");
  }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>

<body>
  <body>
      <div class="container">

                  <div class="span10 offset1">
                      <div class="row">
                          <h3>Créer un compte</h3>
                      </div>

                      <form class="form-horizontal" action="create.php" method="post">
                        <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                          <label class="control-label">Nom</label>
                          <div class="controls">
                              <input name="name" type="text"  placeholder="Nom" value="<?php echo !empty($name)?$name:'';?>">
                              <?php if (!empty($nameError)): ?>
                                  <span class="help-inline"><?php echo $nameError;?></span>
                              <?php endif; ?>
                          </div>
                        </div>

                        <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
                          <label class="control-label">Email</label>
                          <div class="controls">
                              <input name="email" type="text" placeholder="Email" value="<?php echo !empty($email)?$email:'';?>">
                              <?php if (!empty($emailError)): ?>
                                  <span class="help-inline"><?php echo $emailError;?></span>
                              <?php endif;?>
                          </div>
                        </div>

                        <div class="control-group <?php echo !empty($loginError)?'error':'';?>">
                          <label class="control-label">Login</label>
                          <div class="controls">
                              <input name="login" type="text" placeholder="Login" value="<?php echo !empty($login)?$login:'';?>">
                              <?php if (!empty($loginError)): ?>
                                  <span class="help-inline"><?php echo $loginError;?></span>
                              <?php endif;?>
                          </div>
                        </div>

                        <div class="control-group <?php echo !empty($mdpError)?'error':'';?>">
                          <label class="control-label">Mot de passe</label>
                          <div class="controls">
                              <input name="mdp" type="text"  placeholder="Mot de Passe" value="<?php echo !empty($mdp)?$mdp:'';?>">
                              <?php if (!empty($mdpError)): ?>
                                  <span class="help-inline"><?php echo $mdpError;?></span>
                              <?php endif; ?>
                          </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Créer</button>
                            <a class="btn" href="../index.php">Retour</a>
                          </div>
                      </form>
                  </div>

      </div> <!-- /container -->
    </body>
  </body>
</html>
