<?php
require 'autoloader.php';
Autoloader::register();

    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }

    if ( null==$id ) {
        header("Location: crud.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM users where Users_Id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>

<!DOCTYPE html>
<html lang="fr">
<?php include 'head.php' ?>
</head>

<body>
    <div class="container">

                <div class="span10 offset1">
                    <div class="row">
                        <h3>Read a Customer</h3>
                    </div>

                    <div class="form-horizontal" >
                      <div class="control-group">
                        <hr>
                        <label class="control-label"><b>Name</b></label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['Users_Nom'];?>
                            </label>
                        </div>
                      </div>
                      <div class="control-group">
                        <hr>
                        <label class="control-label"><b>Email Address</b></label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['Users_Email'];?>
                            </label>
                        </div>
                      </div>

                      <div class="control-group">
                        <hr>
                        <label class="control-label"><b>Login</b></label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['Users_Login'];?>
                            </label>
                        </div>
                      </div>


                      <div class="control-group">
                        <hr>
                        <label class="control-label"><b>Password</b></label>
                        <div class="controls">
                            <label class="checkbox">
                                <?php echo $data['Users_Mdp'];?>
                            </label>
                        </div>
                      </div>
                      <hr>

                        <div class="form-actions">
                          <a class="btn" href="crud.php">Back</a>
                       </div>


                    </div>
                </div>

    </div> <!-- /container -->
  </body>
</html>
