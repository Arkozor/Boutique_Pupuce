<?php
// j'inclue ici mon fichier commun à toutes mes pages afin de centraliser/factoriser les infos dans un soucis de maintenabilité
include 'AppTop.php';


// traitement du formulaire
if(isset($_POST) && !empty($_POST)){

	// si on utilise la POO
	// je créé un nouveau Users avec les données rentrées dans le formulaire
	$users = new Users($_POST["Login"], $_POST["mdp"]);
	// je serialize mon objet : je génère une représentation stockable d'une valeur
	// C'est une technique pratique pour stocker ou passer des valeurs PHP entre scripts, sans perdre leur structure ni leur type.
	$serialize = serialize($users);

	// je stocke dans un fichier le contenue de $serialize
	// Attention le dossier session que j'utilise (vous pouvez stocker le fichier où vous voulez) doit être accessible en écriture pour tout le monde (chmod 777)
	// Attention le nom du fichier doit être unique de manière certaine
	// Si le nom de fichier est commun a toutes les instances des objets comme c'est le cas ici cela veux dire que tous le monde peux se connecter et donc adieu la sécurité

	// j'ouvre un fichier en écriture
	// je créé un nom de fichier unique que je garde en mémoire
	$_SESSION["sessions"] = sha1(time());
	$fp = fopen("session/".$_SESSION["sessions"],"w");
	// j'écris dedans le contenus de mon objet
	fwrite($fp, $serialize);
	// je ferme mon fichier
	fclose($fp);

	// ATTENTION privilégiez UNE SEULE méthode entre POO et sessions directe : Si vous faîtes de la POO restez en POO!

	// si on utilise les sessions directements
	// en 2 lignes
	//$_SESSION['Users']['login'] = $_POST["Login"];
	//$_SESSION['Users']['mdp'] = $_POST["mdp"];
	// en 1 ligne
	//$_SESSION["Users"] = ["login" => $_POST["Login"] , "mdp" => $_POST["mdp"] ];


	// dans tous les cas je redirige vers la page qui m'intéresse
	header("Location: index.php");
	exit;
}

?>
<form class="form-signin col-6 offset-3" method="post" action="login.php">
	<h1 class="h3 mb-3 font-weight-normal">Connectez-vous</h1>
	<label for="inputEmail" class="sr-only">Login</label>
	<input name="Login" type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus autocomplete="off">
	<label for="inputPassword" class="sr-only">Password</label>
	<input name="mdp" type="password" id="inputPassword" class="form-control" placeholder="Password" required autocomplete="off">
	<br>
	<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
</form>
