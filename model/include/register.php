<?php

    require './include/config.php';

    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
        $emailError = null;
        $loginError = null;
        $mdpError = null;


        // keep track post values
        $name = $_POST['name'];
        $login = $_POST['login'];
        $mdp = $_POST['mdp'];
        $email = $_POST['email'];



        // validate input
        $valid = true;
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }

        if (empty($email)) {
            $emailError = 'Please enter Email Address';
            $valid = false;
        } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
            $emailError = 'Please enter a valid Email Address';
            $valid = false;
        }

        if (empty($login)) {
            $loginError = 'Please enter your Login';
            $valid = false;
        }

        if (empty($mdp)) {
            $mdpError = 'Please enter your Password';
            $valid = false;
        }

        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO users (Users_Nom, Users_Login, Users_Mdp, Users_Email) values(?, ?, ?, ?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($name,$login,$mdp,$email));
            Database::disconnect();
            header("Location: index.php");
        }
    }
?>

<form class="form-horizontal" action="register.php" method="post">
  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
    <label class="control-label">Nom</label>
    <div class="controls">
        <input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
        <?php if (!empty($nameError)): ?>
            <span class="help-inline"><?php echo $nameError;?></span>
        <?php endif; ?>
    </div>
  </div>

  <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
    <label class="control-label">Email</label>
    <div class="controls">
        <input name="email" type="text" placeholder="Email Address" value="<?php echo !empty($email)?$email:'';?>">
        <?php if (!empty($emailError)): ?>
            <span class="help-inline"><?php echo $emailError;?></span>
        <?php endif;?>
    </div>
  </div>

  <div class="control-group <?php echo !empty($loginError)?'error':'';?>">
    <label class="control-label">Login</label>
    <div class="controls">
        <input name="login" type="text" placeholder="Login" value="<?php echo !empty($login)?$login:'';?>">
        <?php if (!empty($loginError)): ?>
            <span class="help-inline"><?php echo $loginError;?></span>
        <?php endif;?>
    </div>
  </div>

  <div class="control-group <?php echo !empty($mdpError)?'error':'';?>">
    <label class="control-label">Mot de passe</label>
    <div class="controls">
        <input name="mdp" type="text"  placeholder="Password" value="<?php echo !empty($mdp)?$mdp:'';?>">
        <?php if (!empty($mdpError)): ?>
            <span class="help-inline"><?php echo $mdpError;?></span>
        <?php endif; ?>
    </div>
  </div>

  <div class="form-actions">
      <button type="submit" class="btn btn-success">Créer</button>
      <a class="btn" href="index.php">Retour</a>
    </div>
</form>
