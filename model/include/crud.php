<?php
require 'autoloader.php';
Autoloader::register();

if ( !empty($_POST)) {
  // keep track validation errors
  $nameError = null;
  $emailError = null;
  $mdpError = null;

  // keep track post values
  $name = $_POST['name'];
  $email = $_POST['email'];
  $mdp = $_POST['mdp'];

  // validate input
  $valid = true;
  if (empty($name)) {
    $nameError = 'Please enter Name';
    $valid = false;
  }

  if (empty($email)) {
    $emailError = 'Please enter Email Address';
    $valid = false;
  } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
    $emailError = 'Please enter a valid Email Address';
    $valid = false;
  }

  if (empty($mdp)) {
    $mdpError = 'Please enter Password';
    $valid = false;
  }

  // insert data
  if ($valid) {
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO users (Users_Nom,Users_Login,Users_Email,Users_Mdp) values(?, ?, ?, ?)";
    $q = $pdo->prepare($sql);
    $q->execute(array($name,$login,$email,$mdp));
    Database::disconnect();
    header("Location: ../control/index.php");
  }
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include 'head.php' ?>
</head>

<body>
  <div class="container">
    <div class="row">
      <h3>Créer un utilisateur</h3>
    </div>
    <div class="row">
      <p>
        <a id="create" href="create.php" class="btn btn-success">Créer</a>
      </p>

      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Login</th>
            <th>Email Address</th>
            <th>Password</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $pdo = Database::connect();
          $sql = 'SELECT * FROM users ORDER BY Users_Id DESC';
          foreach ($pdo->query($sql) as $row) {
            echo '<tr>';
            echo '<td>'. $row['Users_Id'] . '</td>';
            echo '<td>'. $row['Users_Nom'] . '</td>';
            echo '<td>'. $row['Users_Email'] . '</td>';
            echo '<td>'. $row['Users_Login'] . '</td>';
            echo '<td>'. $row['Users_Mdp'] . '</td>';
            echo '<td width=250>';
            echo '<a class="btn btn-info" href="read.php?id='.$row['Users_Id'].'">Consulter</a>';
            echo ' ';
            echo '<a class="btn btn-success" href="update.php?id='.$row['Users_Id'].'">Mettre à jour</a>';
            echo ' ';
            echo '<a class="btn btn-danger" href="delete.php?id='.$row['Users_Id'].'">Supprimer</a>';
            echo '</td>';
            echo '</tr>';
          }
          Database::disconnect();
          ?>
        </tbody>
      </table>
      <a class="btn btn-info" href="../../control/index.php">Retourner à l'accueil</a>
    </div>
  </div> <!-- /container -->
  </html>
