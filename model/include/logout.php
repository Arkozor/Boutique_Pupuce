<?php
//je lance ma session
session_start();
// je détruit ma session
session_destroy();
// si j'utilise la POO je détruit le fichier de session
unlink("session/".$_SESSION["sessions"]);
// je redirige vers une page de mon site
header("Location: ../../control/index.php");
exit;
?>

<form class="form-signin col-6 offset-3" method="post" action="login.php">
	<h1 class="h3 mb-3 font-weight-normal">Deco</h1>
	<label for="inputEmail" class="sr-only">Login</label>
	<input name="Login" type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus autocomplete="off">
	<label for="inputPassword" class="sr-only">Password</label>
	<input name="mdp" type="password" id="inputPassword" class="form-control" placeholder="Password" required autocomplete="off">
	<br>
	<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
</form>
