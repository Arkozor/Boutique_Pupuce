<?php
require 'autoloader.php';
Autoloader::register(); 

    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }

    if ( null==$id ) {
        header("Location: crud.php");
    }

    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
        $loginError = null;
        $emailError = null;
        $mdpError = null;


        // keep track post values
        $name = $_POST['name'];
        $login = $_POST['login'];
        $email = $_POST['email'];
        $mdp = $_POST['mdp'];

        // validate input
        $valid = true;
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }

        if (empty($email)) {
            $emailError = 'Please enter Email Address';
            $valid = false;
        } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
            $emailError = 'Please enter a valid Email Address';
            $valid = false;
        }

        if (empty($login)) {
            $loginError = 'Please enter your Login';
            $valid = false;
        }

        if (empty($mdp)) {
            $mdpError = 'Please enter your Password';
            $valid = false;
        }

        // update data
        if ($valid) {
          $pdo = Database::connect();
          $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "UPDATE users  set Users_Nom = ?, Users_Login = ?, Users_Email = ?, Users_Mdp = ? WHERE Users_id = ?";
          $q = $pdo->prepare($sql);
          $q->execute(array($name,$login,$email,$mdp,$id));
          Database::disconnect();
          header("Location: crud.php");
        }
    } else {
      $pdo = Database::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "SELECT * FROM users where Users_Id = ?";
      $q = $pdo->prepare($sql);
      $q->execute(array($id));
      $data = $q->fetch(PDO::FETCH_ASSOC);
      $name = $data['Users_Nom'];
      $login = $data['Users_Login'];
      $email = $data['Users_Email'];
      $mdp = $data['Users_Mdp'];
      Database::disconnect();
    }
?>
<!DOCTYPE html>
<html lang="fr">
<?php include 'head.php' ?>
</head>

<body>
    <div class="container">

                <div class="span10 offset1">
                    <div class="row">
                        <h3>Update a Customer</h3>
                    </div>

                    <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">
                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <span class="help-inline"><?php echo $nameError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>

                      <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
                        <label class="control-label">Email Address</label>
                        <div class="controls">
                            <input name="email" type="text" placeholder="Email Address" value="<?php echo !empty($email)?$email:'';?>">
                            <?php if (!empty($emailError)): ?>
                                <span class="help-inline"><?php echo $emailError;?></span>
                            <?php endif;?>
                        </div>
                      </div>

                      <div class="control-group <?php echo !empty($loginError)?'error':'';?>">
                        <label class="control-label">Login</label>
                        <div class="controls">
                            <input name="login" type="text" placeholder="Login" value="<?php echo !empty($login)?$login:'';?>">
                            <?php if (!empty($loginError)): ?>
                                <span class="help-inline"><?php echo $loginError;?></span>
                            <?php endif;?>
                        </div>
                      </div>

                      <div class="control-group <?php echo !empty($mdpError)?'error':'';?>">
                        <label class="control-label">Password</label>
                        <div class="controls">
                            <input name="mdp" type="text"  placeholder="Password" value="<?php echo !empty($mdp)?$mdp:'';?>">
                            <?php if (!empty($mdpError)): ?>
                                <span class="help-inline"><?php echo $mdpError;?></span>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Update</button>
                          <a class="btn" href="crud.php">Back</a>
                        </div>
                    </form>
                </div>

    </div> <!-- /container -->
  </body>
</html>
