<?php
require_once ("../model/config.php");

//HEAD
include("../model/head.php");?>
<body>

  <!-- HEADER -->
  <?php include("../view/header.php"); ?>

  <!-- MAIN CONTAINER OF THE WEBSITE -->
  <div id="mainContainer" class="container">
    <div class="row">

      <!-- LEFT PART OF THE MAIN CONTAINER -->
      <div id="leftContainer" class="col-8">
        <?php include("../view/leftContainer.php"); ?>
      </div>

      <!-- RIGHT PART OF THE MAIN CONTAINER -->
      <div id="rightContainer" class="col-4">
        <?php include("../view/rightContainer.php"); ?>
      </div>
    </div>

      <!-- GAME -->
    <?php include("../view/jeu.php"); ?>
  </div>


    <!-- SCRIPTS -->
  <script src="../model/jeuScripts/var&interface.js"></script>
  <script src="../model/jeuScripts/attaques.js"></script>
  <script src="../model/jeuScripts/ennemi.js"></script>

</body>
</html>
