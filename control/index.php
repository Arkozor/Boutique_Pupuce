<?php
session_start();

//HEAD
include("../model/include/head.php");
require_once ("../model/include/class/Users.php");

?>

<body>

  <!-- HEADER -->
  <?php include("../view/header.php"); ?>

  <!-- MAIN CONTAINER OF THE WEBSITE -->
  <div id="mainContainer" class="container">
    <div class="row">

      <!-- LEFT PART OF THE MAIN CONTAINER -->
      <div id="leftContainer" class="col-8">
        <?php include("../view/leftContainer.php"); ?>
      </div>

      <!-- RIGHT PART OF THE MAIN CONTAINER -->
      <div id="rightContainer" class="col-4">
        <?php include("../view/rightContainer.php"); ?>
      </div>
    </div>

    <!-- GAME -->
    <div id="JeuContainer">
      <?php include("../view/jeu.php"); ?>
    </div>
  </div>




  <!-- SCRIPTS -->
  <!-- <script src="../view/scripts.js"></script> -->
  <script src="../view/scripts/index.js"></script>
  <script src="../model/jeuScripts/var&interface.js"></script>
  <script src="../model/jeuScripts/attaques.js"></script>
  <script src="../model/jeuScripts/ennemi.js"></script>

</body>
</html>
